# Welcome To The Family
Dvk is a family of crates providing definitions and dynamic loaders for core Vulkan API and it's extensions. Dynamic loading is the correct way to load Vulkan. Every crate in Dvk family loads Vulkan commands dynamically in two stages, first loading global commands, then instance specific commands, ensuring in this way a correct operation even on the most conservative of systems. Changes to the API interface where they were dictated by language differences or the requirement to separate extensions into their own crates are introduced sparingly and in such a way that they will not alter or limit the functionality of the API as outlined by the official standard(s).

# About This Crate
This crate provides the definitions and a dynamic loader for vk_ext_debug_report extension to Vulkan API. This crate does not export any command prototypes. To get access to commands you have to create a new instance of VulkanExtDebugReport struct, by calling VulkanExtDebugReport::new(). To load the 3 commands added by this extension you have to pass an instance object to load method of VulkanExtDebugReport. Current thread will panic if you attempt to call any of instance-specific functions before they were loaded.

This crate contains extensions to VkResult and VkStructureType enums from core Vulkan API in the form of VkExtDebugReportResult and VkExtDebugReportStructureType enums respectively. Values of these types are limited in scope to be used only within this extension, no conversion is provided to cast to original VkStructureType or VkResult types.


