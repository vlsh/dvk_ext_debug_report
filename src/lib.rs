#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(dead_code)]

#[macro_use]
extern crate bitflags;
extern crate libc;
extern crate shared_library;
#[macro_use]
extern crate dvk;

use self::libc::{c_void, c_char, uint32_t, size_t, uint64_t, c_float, int32_t, uint8_t};
use self::shared_library::dynamic_library::DynamicLibrary;
use std::path::{Path};
use std::ffi::CString;
use dvk::{VkInstance, VkFlags, VkAllocationCallbacks, VkBool32, vkVoidFunctionFn, vkGetInstanceProcAddrFn};

VK_DEFINE_NON_DISPATCHABLE_HANDLE!(VkDebugReportCallbackEXT);

pub const VK_EXT_DEBUG_REPORT_SPEC_VERSION: uint32_t = 2;
pub const VK_EXT_DEBUG_REPORT_EXTENSION_NAME: *const c_char = b"VK_EXT_debug_report\0" as *const u8 as *const c_char;
pub const VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT: VkExtDebugReportStructureType = VkExtDebugReportStructureType::VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;

// WARNING: It's not documented anywhere what VkResult values vkCreateDebugReportCallbackEXTFn may return
#[repr(i32)]
#[derive(Eq)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum VkExtDebugReportResult {
    VK_SUCCESS = 0,
    VK_ERROR_OUT_OF_HOST_MEMORY = -1,
    VK_ERROR_OUT_OF_DEVICE_MEMORY = -2,
    VK_ERROR_VALIDATION_FAILED_EXT = -1000011001
}

#[repr(i32)]
#[derive(Eq)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum VkExtDebugReportStructureType {
    VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT = 1000011000
}

#[repr(i32)]
#[derive(Eq)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum VkDebugReportObjectTypeEXT {
    VK_DEBUG_REPORT_OBJECT_TYPE_UNKNOWN_EXT = 0,
    VK_DEBUG_REPORT_OBJECT_TYPE_INSTANCE_EXT = 1,
    VK_DEBUG_REPORT_OBJECT_TYPE_PHYSICAL_DEVICE_EXT = 2,
    VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_EXT = 3,
    VK_DEBUG_REPORT_OBJECT_TYPE_QUEUE_EXT = 4,
    VK_DEBUG_REPORT_OBJECT_TYPE_SEMAPHORE_EXT = 5,
    VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_BUFFER_EXT = 6,
    VK_DEBUG_REPORT_OBJECT_TYPE_FENCE_EXT = 7,
    VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_MEMORY_EXT = 8,
    VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT = 9,
    VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_EXT = 10,
    VK_DEBUG_REPORT_OBJECT_TYPE_EVENT_EXT = 11,
    VK_DEBUG_REPORT_OBJECT_TYPE_QUERY_POOL_EXT = 12,
    VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_VIEW_EXT = 13,
    VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_VIEW_EXT = 14,
    VK_DEBUG_REPORT_OBJECT_TYPE_SHADER_MODULE_EXT = 15,
    VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_CACHE_EXT = 16,
    VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_LAYOUT_EXT = 17,
    VK_DEBUG_REPORT_OBJECT_TYPE_RENDER_PASS_EXT = 18,
    VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_EXT = 19,
    VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT_EXT = 20,
    VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_EXT = 21,
    VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_POOL_EXT = 22,
    VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_EXT = 23,
    VK_DEBUG_REPORT_OBJECT_TYPE_FRAMEBUFFER_EXT = 24,
    VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_POOL_EXT = 25,
    VK_DEBUG_REPORT_OBJECT_TYPE_SURFACE_KHR_EXT = 26,
    VK_DEBUG_REPORT_OBJECT_TYPE_SWAPCHAIN_KHR_EXT = 27,
    VK_DEBUG_REPORT_OBJECT_TYPE_DEBUG_REPORT_EXT = 28
}

#[repr(i32)]
#[derive(Eq)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum VkDebugReportErrorEXT {
    VK_DEBUG_REPORT_ERROR_NONE_EXT = 0,
    VK_DEBUG_REPORT_ERROR_CALLBACK_REF_EXT = 1
}

bitflags! { 
    pub flags VkDebugReportFlagBitsEXT: VkFlags {
    	const VK_DEBUG_REPORT_INFORMATION_BIT_EXT = 0x00000001,
    	const VK_DEBUG_REPORT_WARNING_BIT_EXT = 0x00000002,
    	const VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT = 0x00000004,
    	const VK_DEBUG_REPORT_ERROR_BIT_EXT = 0x00000008,
    	const VK_DEBUG_REPORT_DEBUG_BIT_EXT = 0x00000010
    }
}
pub type VkDebugReportFlagsEXT = VkFlags;

pub type vkDebugReportCallbackEXTFn = unsafe extern "stdcall" fn(flags: VkDebugReportFlagsEXT,
                                                                 objectType: VkDebugReportObjectTypeEXT,
                                                                 object: uint64_t,
                                                                 location: size_t,
                                                                 messageCode: int32_t,
                                                                 pLayerPrefix: *const c_char,
                                                                 pMessage: *const c_char,
                                                                 pUserData: *mut c_void) -> VkBool32;
#[repr(C)]
pub struct VkDebugReportCallbackCreateInfoEXT {
    pub sType: VkExtDebugReportStructureType,
    pub pNext: *const c_void,
    pub flags: VkDebugReportFlagsEXT,
    pub pfnCallback: vkDebugReportCallbackEXTFn,
    pub pUserData: *mut c_void
}

pub type vkCreateDebugReportCallbackEXTFn = unsafe extern "stdcall" fn(instance: VkInstance,
                                                                       pCreateInfo: *const VkDebugReportCallbackCreateInfoEXT,
                                                                       pAllocator: *const VkAllocationCallbacks, 
                                                                       pCallback: *mut VkDebugReportCallbackEXT) -> VkExtDebugReportResult;

pub type vkDestroyDebugReportCallbackEXTFn = unsafe extern "stdcall" fn(instance: VkInstance,
                                                                        callback: VkDebugReportCallbackEXT,
                                                                        pAllocator: *const VkAllocationCallbacks);

pub type vkDebugReportMessageEXTFn = unsafe extern "stdcall" fn(instance: VkInstance,
                                                                flags: VkDebugReportFlagsEXT,
                                                                objectType: VkDebugReportObjectTypeEXT,
                                                                object: uint64_t,
                                                                location: size_t,
                                                                messageCode: int32_t,
                                                                pLayerPrefix: *const c_char,
                                                                pMessage: *const c_char);

#[cfg(windows)]
static VULKAN_LIBRARY: &'static str = "vulkan-1.dll";

#[cfg(unix)]
static VULKAN_LIBRARY: &'static str = "libvulkan-1.so";

#[derive(Default)]
pub struct VulkanExtDebugReport {
   library: Option<DynamicLibrary>,
   vkGetInstanceProcAddr: Option<vkGetInstanceProcAddrFn>,
   vkCreateDebugReportCallbackEXT: Option<vkCreateDebugReportCallbackEXTFn>,
   vkDestroyDebugReportCallbackEXT: Option<vkDestroyDebugReportCallbackEXTFn>,
   vkDebugReportMessageEXT: Option<vkDebugReportMessageEXTFn>
}

impl VulkanExtDebugReport {
    pub fn new() -> Result<VulkanExtDebugReport, String> {
        let mut vulkan_ext_debug_report: VulkanExtDebugReport = Default::default();
        let library_path = Path::new(VULKAN_LIBRARY);
        vulkan_ext_debug_report.library = match DynamicLibrary::open(Some(library_path)) {
            Err(error) => return Err(format!("Failed to load {}: {}",VULKAN_LIBRARY,error)),
            Ok(library) => Some(library),
        };
        unsafe {
            vulkan_ext_debug_report.vkGetInstanceProcAddr = Some(std::mem::transmute(try!(vulkan_ext_debug_report.library.as_ref().unwrap().symbol::<u8>("vkGetInstanceProcAddr"))));
        }
        Ok(vulkan_ext_debug_report)
    }

    unsafe fn load_command(&self, instance: VkInstance, name: &str) -> Result<vkVoidFunctionFn, String> {
        let fn_ptr = (self.vkGetInstanceProcAddr.as_ref().unwrap())(instance, CString::new(name).unwrap().as_ptr());
        if fn_ptr != std::ptr::null() {
            Ok(fn_ptr)
        } else {
            Err(format!("Failed to load {}",name))
        }
    }

    pub fn load(&mut self, instance: VkInstance) -> Result<(), String> {
        unsafe {
            self.vkCreateDebugReportCallbackEXT = Some(std::mem::transmute(try!(self.load_command(instance, "vkCreateDebugReportCallbackEXT"))));
            self.vkDestroyDebugReportCallbackEXT = Some(std::mem::transmute(try!(self.load_command(instance, "vkDestroyDebugReportCallbackEXT"))));
            self.vkDebugReportMessageEXT = Some(std::mem::transmute(try!(self.load_command(instance, "vkDebugReportMessageEXT"))));
        }
        Ok(())
    }

    pub unsafe fn vkCreateDebugReportCallbackEXT(&self,
                                                 instance: VkInstance,
                                                 pCreateInfo: *const VkDebugReportCallbackCreateInfoEXT,
                                                 pAllocator: *const VkAllocationCallbacks, 
                                                 pCallback: *mut VkDebugReportCallbackEXT) -> VkExtDebugReportResult {
        (self.vkCreateDebugReportCallbackEXT.as_ref().unwrap())(instance, pCreateInfo, pAllocator, pCallback)
    }

    pub unsafe fn vkDestroyDebugReportCallbackEXT(&self,
                                                  instance: VkInstance,
                                                  callback: VkDebugReportCallbackEXT,
                                                  pAllocator: *const VkAllocationCallbacks) {
        (self.vkDestroyDebugReportCallbackEXT.as_ref().unwrap())(instance, callback, pAllocator)
    }

    pub unsafe fn vkDebugReportMessageEXT(&self,
                                          instance: VkInstance,
                                          flags: VkDebugReportFlagsEXT,
                                          objectType: VkDebugReportObjectTypeEXT,
                                          object: uint64_t,
                                          location: size_t,
                                          messageCode: int32_t,
                                          pLayerPrefix: *const c_char,
                                          pMessage: *const c_char) {
        (self.vkDebugReportMessageEXT.as_ref().unwrap())(instance, flags, objectType, object, location, messageCode, pLayerPrefix, pMessage)
    }
}
